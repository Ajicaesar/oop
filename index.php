<?php 
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("Shaun", 4, "no");

echo $sheep->get_name(); // "shaun"
echo $sheep->get_legs(); // 4
echo $sheep->get_cold_blooded(); // "no"


echo "<br>";
$kodok = new Frog("Buduk", 4 , "no" );
echo $kodok->get_name();
echo $kodok->get_legs();
echo $kodok->get_cold_blooded();
echo $kodok->get_yell_frog(); // "Auooo"

echo "<br>";
echo "<br>";

// index.php

$sungokong = new Ape("kera sakti", 2, "no");
echo $sungokong->get_name();
echo $sungokong->get_legs();
echo $sungokong->get_cold_blooded();
echo $sungokong->get_yell_kera(); // "Auooo"

?>